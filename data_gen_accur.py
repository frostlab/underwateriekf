from src.system import HoveringAUV
from src.iekf import IEKF
from src.qekf import QEKF

import numpy as np
from scipy.linalg import expm, block_diag

import argparse
from tqdm import tqdm

from multiprocessing import Pool
from functools import partial
import os
import pickle

def rel_error(x, x0):
        return np.abs( (x - x0) )
        
# iterate through monte carlo sim
#, sys, init_cov, u, z1, z2, sim_bias
def iterate(i_start, dir, init_cov, sys, x, u, z1, z2, sim_bias):
    i = i_start[0]
    start = i_start[1]
    vals = []
    x0 = expm( sys.carat( start ) ) @ x[0]
    results = {'x0': x0}

    # Run the IEKF and plot everything
    iekf = IEKF(sys, x0, init_cov)
    x_result, bias_result, sigmas = iekf.iterate(*u, *z1, *z2, sim_bias, use_tqdm=False)
    results['iekf'] = x_result

    # Run the QEKF and plot everything
    # convert covariance to QEKF covariance
    R = x[0][:3,:3]
    v_cross = HoveringAUV.cross( x[0,:3,3] )
    p_cross = HoveringAUV.cross( x[0,:3,4] )
    temp = init_cov
    init_cov_qekf = block_diag( R.T@temp[:3,:3]@R,
                                temp[3:6,3:6] + v_cross@temp[:3,:3]@v_cross.T,
                                temp[6:9,6:9] + p_cross@temp[:3,:3]@p_cross.T,
                                temp[9:12,9:12],
                                temp[12:15,12:15] )
    qekf = QEKF(sys, x0, init_cov_qekf)
    x_result, bias_result, sigmas = qekf.iterate(*u, *z1, *z2, sim_bias, use_tqdm=False)
    results['qekf'] = x_result

    outfile = f'results/{dir}/run{i}.pkl'
    pickle.dump(results, open(outfile, 'wb'))


def main(data, sim_bias, sec, num, cov_scale):
    ############################     SETUP SYSTEM    ############################
    from src.constants import Q, R, dvl_p, dvl_r, imuHz, dvlHz, depthHz

    # generate data
    # if it's a previous run with data already ran
    if os.path.splitext(data)[1] == ".pkl":
        # open a dummy file here
        sys = HoveringAUV(Q, R, 'data/data.npz', dvl_p, dvl_r)
        file = pickle.load(open(data, 'rb'))
        x = file['x']
        u = file['u']
        z1 = file['z1']
        z2 = file['z2']
        t = u[1]
        sys.T = len(t)
    # or if it's data from holodeck and needs noise added to it
    elif os.path.splitext(data)[1] == ".npz":
        sys = HoveringAUV(Q, R, data, dvl_p, dvl_r)
        t = sys.Hz * sec
        (x, t), u, (bias, _), z1, z2 = sys.gen_data(t, imuHz, dvlHz, depthHz, noise=True, sim_bias=sim_bias)


    ############################     ITERATE     ############################
    # Set up values
    init_cov = (np.diag([np.pi/6, np.pi/6, np.pi/6, 2, 2, 2, 1, 1, 1,
                        .005, .005, .005, .05, .05, .05])*cov_scale)**2

    # make sure we can make output directory
    dir = f'accur_{os.path.splitext(os.path.split(data)[-1])[0]}_cov{cov_scale}'
    os.makedirs(os.path.join('results', dir), exist_ok=True)

    # save trajectory data
    results = {'x': x, 'u': u, 'z1': z1, 'z2': z2}
    outfile = f'results/{dir}/actual.pkl'
    pickle.dump(results, open(outfile, 'wb'))

    # iterate through each time
    delattr(sys, "data")
    start = np.random.multivariate_normal(mean=np.zeros(9), cov=init_cov[:9,:9], size=num)
    with Pool(os.cpu_count()-1) as p:
        vals = list(tqdm( p.imap(partial(iterate, dir=dir, init_cov=init_cov, sys=sys, x=x, u=u, z1=z1, z2=z2, sim_bias=sim_bias), zip(range(num), start)), total=num))
        
if __name__ == "__main__":
    # Parse through arguments
    parser = argparse.ArgumentParser(description="HoveringAUV IEKF")
    parser.add_argument("-d", "--data", type=str, default="data.npz", help="File to read data from. Default data.npz.")
    parser.add_argument("-b", "--sim_bias", action="store_true", help="Include if bias should be simulated and tracked.")
    parser.add_argument("-n", "--num", type=int, default="10", help="Number of iterations to run.")
    parser.add_argument("-s", "--sec", type=int, default="100", help="Number of seconds to run filters on.")
    parser.add_argument("-c", "--cov_scale", type=float, default="1", help="How much to scale initial covariance by.")
    args = vars(parser.parse_args())
    main(**args)