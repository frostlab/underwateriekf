# UnderwaterIEKF
Implementation of the IEKF on underwater vehicles. The IEKF takes in IMU measurements as controls and DVL velocity and pressure depth measurements as measurements.

## Installation
All required dependencies are found in the `requirements.txt` file. Simply run `pip install -r requirements.txt` to install them. Beyond the custom HoloOcean install, they're all pretty basic like numpy, matplotlib, etc.

## Runnings Sims
`data_gen_holodeck.py` will download and run a HoloOcean simlation of our HoveringAUV. The controls are:

| Key | Forward Key | Backward Key |
|---|:---:|:---:|
| Up/Down | i | k |
| Yaw Left/Right | j | k |
| Forward/Backward| w | s |
| Strafe Left/Right | a | d |

To begin recording data, press `r`. To stop the simulation, press `q`. There's a number of flags you can pass to the simulation, to see them run `python holo_datagen.py --help`. 

## Overview of Source Files
`system.py` holds all the info about dynamics, adding noise to the simulation, etc.

`iekf.py` holds the actual InEKF.

`qekf.py` holds the actual QEKF.

`run_iekf.py`/`run_qekf.py` runs their respective filters. Similarly to the simulation, it can take a bunch of flags. Run `python run_blank.py --help` to see them.

I've done my best to add comments to all the code, let me know if any of it is unclear.

## Overview of Figure Files
All figure files have various flags that you can pass to them as well, you can see them via `python figure_myfig.py --help`. The figure they correspond to in the paper is in the title name. To generate all figures as seen in the paper, simply run `bash gen_all_figures.sh`, which will run things just how they were ran in the paper, and save the figures in the figures directory.

## Stored Data
There is some data stored that was used in the paper. It is as follows:

`data/data.npz` is a standard AUV run. It's somewhat short (~18s), and is used for the convergence and timing figures. No noise added.

`data/long1.npz` and `data/long2.npz` are 100s simulations with no noise added. 

`data/long1_fair_noise.pkl` and `data/long2_fair_noise.pkl` are the above simulations with noise added. Various attempts were made to save noise that evened out the low covariance runs used in the accuracy figure. The former in the end was chosen for use in the accuracy figure.
