import numpy as np
from scipy.spatial.transform import Rotation

# Setup rates of sensors
imuHz = 200
dvlHz = 20
depthHz = 100

# Set up various noises 
# std         - spec *    units    *  sqrt rate
std_a         =  20  * 10**-6*9.8 * np.sqrt(imuHz)
std_gyro      = .005 *  np.pi/180  * np.sqrt(imuHz) 
std_dvl       = .0101*     2.6
std_depth     =  51  *    1/100    *    1/2
std_a_bias    = .0001              * np.sqrt(imuHz) # See https://arxiv.org/pdf/1402.5450.pdf
std_gyro_bias = .000618*   8/18    * np.sqrt(imuHz)
Q = np.diag([std_gyro, std_gyro, std_gyro, std_a, std_a, std_a, 0, 0, 0, 
            std_gyro_bias, std_gyro_bias, std_gyro_bias, std_a_bias, std_a_bias, std_a_bias])**2
R = np.diag([std_dvl, std_dvl, std_dvl, 
            std_depth])**2

# DVL offsets
dvl_p = np.array([-0.17137, 0.00922, -0.33989])
dvl_r = Rotation.from_euler('xyz', [6, 3, 90], degrees=True).as_matrix()

