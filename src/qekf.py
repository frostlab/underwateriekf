import numpy as np
from numpy.linalg import inv
from scipy.linalg import expm
from tqdm import tqdm

class QEKF:
    def __init__(self, system, mu_0, sigma_0):
        """The semi-newfangled Quaternion (Multiplicative) Extended Kalman Filter

        Args:
            system      (class) : The system to run the Quaternion-EKF (Multiplicative EKF) on. It will pull Q, R, f, h from this.
            mu0     (nxn array) : Initial starting point of system
            sigma0  (mxm array) : Initial covariance of system"""
        if mu_0.shape == (9,):
            mu_0 = expm( system.carat(mu_0) )

        self.sys = system
        self.mus = [mu_0]
        self.sigmas = [sigma_0]
        self.biass = [np.zeros((2,3))]

        self.invR = np.zeros((3,3))
        self.invR[-1,-1] = 1 / self.sys.R[3,3]
    
    # These 3 properties are helpers. Since our mus and sigmas are stored in a list
    # it can be a pain to access the last one. These provide "syntactic sugar"
    @property
    def mu(self):
        return self.mus[-1]

    @property
    def sigma(self):
        return self.sigmas[-1]

    @property
    def bias(self):
        return self.biass[-1]

    def predict(self, u, dt):
        """Runs prediction step of Quaternion-EKF. (Multiplicative EKF)

        Args:
            u       (k ndarray) : control taken at this step
            u[0]                : angular velocity
            u[1]                : acceleration

        Returns:
            mu    (nxn ndarray) : Propagated state
            mu[:3,:3]           : Rotation Matrix
            mu[:3,3]            : Velocity
            mu[:3,4]            : Position (z, what we care about the most because of the depth measurement, is mu[2,4])

            sigma (nxn ndarray) : Propagated covariances"""
        
        # Update the state
        mu_bar = self.sys.f_lie(self.mu, u, dt, self.bias)

        # make propagation matrix
        zero    = np.zeros((3,3))
        I       = np.eye(3)

        # Get A matrix
        w_cross = self.sys.cross( u[0] - self.bias[0] )
        a_cross = self.sys.cross( u[1] - self.bias[1] )
        R = mu_bar[:3,:3]
        self.expA = expm( np.block([[  -w_cross,     zero,     zero,   -I, zero],
                                    [-R@a_cross,     zero,     zero, zero,   -R],
                                    [      zero,        I,     zero, zero, zero],
                                    [      zero,     zero,     zero, zero, zero],
                                    [      zero,     zero,     zero, zero, zero]])*dt )
        # self.expA = np.block([[expm(-w_cross*dt), zero, zero, -I*dt,  zero],
        #                       [    -R@a_cross*dt,    I, zero,  zero, -R*dt],
        #                       [             zero, I*dt, I,     zero,  zero],
        #                       [             zero, zero, zero,     I,  zero],
        #                       [             zero, zero, zero,  zero,     I]])
        
        # Multiply elements of Q by dt and dt^2, as appropriate 
        Qdt = self.sys.Q * dt
        Qdt[:6,:6] = Qdt[:6,:6] * dt
        # Update the covariance
        sigma_bar = self.expA @ self.sigma @ self.expA.T + Qdt

        #save for use later
        self.last_u = u
        self.mus.append( mu_bar )
        self.biass.append( self.bias )
        self.sigmas.append( sigma_bar )

        return mu_bar, sigma_bar

    def update_depth(self, z, sim_bias=True):
        """Runs correction step of Quaternion-EKF. (Multiplicative EKF)

        Args:
            z (m ndarray): measurement at this step, corresponds to mu[2,4]

        Returns:
            mu    (nxn ndarray) : Corrected state
            sigma (nxn ndarray) : Corrected covariances"""
        # make H matrices
        zero    = np.zeros((1,3))
        I       = np.eye(3)    
        H = np.block([zero, zero, I[-1,:], zero, zero])
        # make innovation
        z_hat = self.mu[2,4]
        V = z - z_hat

        # make our special measurement covariance
        R = self.mu[:3,:3] # Rotation Matrix
        meas_cov = inv(H@self.sigma@H.T + self.sys.R[-1,-1])

        K = self.sigma @ H.T @ meas_cov
        K_state = K[:9]
        K_bias = K[9:]

        K_V = (K * V).flatten()
        # Update the state
        self.mus[-1][:3,:3] = self.mu[:3,:3] @ expm( self.sys.cross( K_V[:3] ) )
        self.mus[-1][:3,3] += K_V[3:6]
        self.mus[-1][:3,4] += K_V[6:9]
        # Update the bias'
        if sim_bias:
            self.biass[-1] = self.bias + ( K_V[9:] ).reshape((2,3))
        # Update the covariance
        self.sigmas[-1] = (np.eye(15) - K @ H) @ self.sigma

        return self.mu, self.sigma

    def update_dvl(self, z, sim_bias=True):
        """Runs correction step of Quaternion-EKF. (Multiplicative EKF)

        Args:
            z (m ndarray): measurement at this step

        Returns:
            mu    (nxn ndarray) : Corrected state
            sigma (nxn ndarray) : Corrected covariances"""
        # convert dvl into correct frame
        z = self.sys.dvl_r@z + self.sys.dvl_p@(self.last_u[0] - self.bias[0])

        # make H matrix
        zero    = np.zeros((3,3))
        I       = np.eye(3)    
        R = self.mu[:3,:3] # Rotation Matrix
        v_hat = self.mu[:3,3] # Velocity
        H = np.block([self.sys.cross( R.T@v_hat ), R.T, zero, zero, zero])

        # make innovation
        z_hat = R.T@v_hat
        V = z - (R.T@v_hat)

        # make our special measurement covariance
        meas_cov   = inv(H@self.sigma@H.T + self.sys.R[:3,:3])
        # Calculate the Kalman Gain
        K = self.sigma @ H.T @ meas_cov

        K_V = K @ V
        # Update State Estimate
        self.mus[-1][:3,:3] = self.mu[:3,:3] @ expm( self.sys.cross( K_V[:3] ) )
        self.mus[-1][:3,3] += K_V[3:6]
        self.mus[-1][:3,4] += K_V[6:9]
        # Update bias'
        if sim_bias:
            self.biass[-1] = self.bias + ( K_V[9:] ).reshape((2,3))
        # Update the Covariance
        self.sigmas[-1] = (np.eye(15) - K @ H) @ self.sigma
        
        return self.mu, self.sigma

    def iterate(self, u, tu, z1, tz1, z2, tz2, sim_bias=True, use_tqdm=True):
        """Given a sequence of observation, iterate through EKF
        
        Args:
            us (txk ndarray) : controls for each step, each of size k
            zs (txm ndarray) : measurements for each step, each of size m
            
        Returns:
            mus    (txnxn ndarray) : resulting states
            sigmas (txnxn ndarray) : resulting sigmas"""
        ui = 0
        z1i = 0
        z2i = 0
        t_range = np.linspace(0, self.sys.T/self.sys.Hz, self.sys.T)
        
        # append one at the end, so after they're done they won't keep running
        tu = np.append(tu, t_range[-1]+1)
        tz1 = np.append(tz1, t_range[-1]+1)
        tz2 = np.append(tz2, t_range[-1]+1)

        dt = 0
        for t in tqdm(t_range, disable=(not use_tqdm)):
            # Run the prediction step
            if tu[ui] <= t:
                if ui != 0:
                    dt = tu[ui] - tu[ui-1]
                self.predict(u[ui], dt)
                if ui < len(tu)-1:
                    ui += 1
            # Run the update for DVL measurements
            if tz1[z1i] <= t:
                self.update_dvl(z1[z1i], sim_bias)
                if z1i < len(tz1)-1:
                    z1i += 1
            # # Run the update for depth measurements
            if tz2[z2i] <= t:
                self.update_depth(z2[z2i], sim_bias)
                if z2i < len(tz2)-1:
                    z2i += 1

        return np.array(self.mus)[1:], np.array(self.biass)[1:], np.array(self.sigmas)[1:]

