import numpy as np
from scipy.linalg import expm
from numpy.linalg import inv
import warnings 
   
class HoveringAUV:
    def __init__(self, Q, R, filename, dvl_p=np.zeros(3), dvl_r=np.eye(3)):
        """Our system for a quadcopter, with IMU measurements as controls. 

        Args:
            filename      (str) : Where the recorded data has been stored
            Q   (15,15 ndarray) : Covariance of noise on state
            R     (6,6 ndarray) : Covariance of noise on measurements
            dvl_p   (3 ndarray) : Translation of DVL from IMU
            dvl_r (3,3 ndarray) : Rotation matrix of DVL from IMU"""
        self.Q = Q
        self.R = R
        if filename is not None:
            self.data = np.load(filename)
        else:
            self.data = None
        self.Hz = self.data['ticks']
        self.T = self.data['x'].shape[0]
        # local velocity
        self.b1 = np.array([0, 0, 0, -1, 0])
        # global depth
        self.b2 = np.array([0, 0, 0, 0, 1])

        self.dvl_p = HoveringAUV.cross(dvl_p)
        self.dvl_r = dvl_r

        # convert z1 noise into the correct frame
        self.R[:3,:3] = self.dvl_r@self.R[:3,:3]@self.dvl_r.T + self.dvl_p@(self.Q[0:3,0:3] + self.Q[9:12,9:12])@self.dvl_p.T

    def gen_data(self, t, imuHz, dvlHz, depthHz, noise=True, sim_bias=True):
        """Generates model data using Lie Group model.

        Args:
            t         (int) : How many timesteps to run
            noise    (bool) : Whether or not to include noise. Defaults to True.
            sim_bias (bool) : Whether or not to include bias. Defaults to True.

        Returns:
            x    (t,5,5 ndarray) : x steps after x0 
            u    (t,2,3 ndarray) : controls that were applied
            bias (t,2,3 ndarray) : bias of IMU (0s if bias is turned off)
            z      (t,3 ndarray) : measurements taken.
        """
        #Get closest sample rate we can
        imu_every = self.Hz // imuHz
        dvl_every = self.Hz // dvlHz
        depth_every = self.Hz // depthHz

        if t > self.T:
            warnings.warn (f"You requested {t} steps, there's only {self.T} available.")
            t = self.T
        t_range = np.linspace(0, self.T/self.Hz, self.T)
        x  = self.data['x'][:t:imu_every]
        u  = self.data['u'][:t:imu_every]
        u[:,[0,1]] = u[:,[1,0]]
        z1 = self.data['z1'][:t:dvl_every]
        z2 = self.data['z2'][:t:depth_every]
        bias = np.zeros_like(u)

        if noise:
            temp = np.random.multivariate_normal(mean=np.zeros(3), cov=self.R[:3,:3], size=z1.shape[0])
            z1 += temp

            temp = np.random.normal(loc=np.zeros(1), scale=np.sqrt(self.R[-1,-1]), size=z2.shape[0])
            z2 += temp

            temp = np.random.multivariate_normal(mean=np.zeros(15), cov=self.Q, size=u.shape[0])
            u[:,0] += temp[:,0:3]
            u[:,1] += temp[:,3:6]
            if sim_bias:
                bias[:,0] += np.cumsum(temp[:,9:12], axis=0)
                bias[:,1] += np.cumsum(temp[:,12:15], axis=0)
                u += bias


        return (x, t_range[:t:imu_every]), (u, t_range[:t:imu_every]), (bias, t_range[:t:imu_every]), (z1, t_range[:t:dvl_every]), (z2, t_range[:t:depth_every])

    def f_lie(self, state, u, dt, bias):
        """Propagates state forward in Lie Group. Used for IEKF.

        Args:
            state (5,5 ndarray) : X_n of model in Lie Group
            u     (2,3 ndarray) : U_n of model (IMU measurements)
            bias  (2,3 ndarray) : Estimated bias

        Returns:
            X_{n+1} (5,5 ndarray)"""
        # transform u into the right representation
        #get stuff we need
        g = np.array([0, 0, -9.8])
        R = state[:3,:3]
        v = state[:3,3]
        p = state[:3,4]

        omega = u[0] - bias[0]
        a = u[1] - bias[1]

        ## put it together
        Rnew = R @ expm( self.cross( omega*dt ))
        vnew = v + (R@a + g)*dt
        pnew = p + v*dt + (R@a + g)*dt**2/2

        return np.block([[Rnew, vnew.reshape(-1,1), pnew.reshape(-1,1)],
                        [np.zeros((2,3)), np.eye(2)]])

    def h(self, state):
        """Calculates measurement given a state. Note that the result is
            the same if it's in standard or Lie Group form, so we simplify into
            one function.
            
        Args:
            state (3,3 ndarray) : Current state in either standard or Lie Group form
            noise        (bool) : Whether or not to add noise. Defaults to False.

        Returns:
            z1 (3 ndarray) : DVL Velocity measurement
            z2   (1 float) : Depth measurement"""
        # local velocity
        z1 = ( inv(state) @ self.b1 )[:3]
        # global depth
        z2 = ( (state) @ self.b2 )[2]

        return z1, z2

    @staticmethod
    def cross(x):
        """Moves a 3 vector into so(3)

        Args:
            x (3 ndarray) : Parametrization of Lie Algebra

        Returns:
            x (3,3 ndarray) : Element of so(3)"""
        return np.array([[   0, -x[2],  x[1]],
                        [ x[2],     0, -x[0]],
                        [-x[1],  x[0],     0]])

    @staticmethod
    def carat(xi):
        """Moves a 9 vector to the Lie Algebra se_2(3).

        Args:
            xi (9 ndarray) : Parametrization of Lie algebra

        Returns:
            xi^ (5,5 ndarray) : Element in Lie Algebra se_2(3)"""
        w_cross = HoveringAUV.cross(xi[0:3])
        v       = xi[3:6].reshape(-1,1)
        p       = xi[6:9].reshape(-1,1)
        return np.block([[w_cross, v, p],
                         [np.zeros((2,5))]])

    @staticmethod
    def adjoint(xi):
        """Takes adjoint of element in SE_2(3)

        Args:
            xi (5,5 ndarray) : Element in Lie Group

        Returns:
            Ad_xi (9,9 ndarray) : Adjoint in SE_2(3)"""
        R = xi[:3,:3]
        v_cross = HoveringAUV.cross(xi[:3,3])
        p_cross = HoveringAUV.cross(xi[:3,4])
        zero    = np.zeros((3,3))
        return np.block([[        R, zero, zero],
                         [v_cross@R,    R, zero],
                         [p_cross@R, zero,   R]])

# we do our testing down here
if __name__ == "__main__":
    import sys
    import matplotlib.pyplot as plt
    from mpl_toolkits.mplot3d import Axes3D
    from scipy.spatial.transform import Rotation
    np.set_printoptions(suppress=True, formatter={'float_kind':f'{{:0.7f}}'.format}) 

    ############################     SETUP SYSTEM    ############################
    # Setup rates of sensors
    imuHz = 200
    dvlHz = 20
    depthHz = 100

    # Set up various noises 
    # std         - spec *    units    *  sqrt rate
    std_a         =  20  * 10**-6*9.8 * np.sqrt(imuHz)
    std_gyro      = .005 *  np.pi/180  * np.sqrt(imuHz) 
    std_dvl       = .0101*     2.6
    std_depth     =  51  *    1/100    *    1/2
    std_a_bias    = .0001              * np.sqrt(imuHz) # See https://arxiv.org/pdf/1402.5450.pdf
    std_gyro_bias = .000618*   8/18    * np.sqrt(imuHz)
    Q = np.diag([std_gyro, std_gyro, std_gyro, std_a, std_a, std_a, 0, 0, 0, 
                std_gyro_bias, std_gyro_bias, std_gyro_bias, std_a_bias, std_a_bias, std_a_bias])**2
    R = np.diag([std_dvl, std_dvl, std_dvl, 
                std_depth])**2

    # DVL offsets
    dvl_p = np.array([-0.17137, 0.00922, -0.33989])
    dvl_r = Rotation.from_euler('xyz', [6, 3, 90], degrees=True).as_matrix()

    #make system
    sys = HoveringAUV(Q, R, 'data/test_move_forward.npz', dvl_p, dvl_r)
    t = sys.T

    # load data, and run it through our system to check they match (ish)
    (x, tx), (u, tu), (bias, tb), (z1, tdvl), (z2, tdepth) = sys.gen_data(t, imuHz, dvlHz, depthHz, noise=False, sim_bias=False)
    x_result = np.zeros((t, 5, 5))
    x_result[0] = x[0]
    dt = 0
    ti_last = 0
    for i, (ti, ui, bi) in enumerate(zip(tu, u, bias)):
        dt = ti - ti_last
        ti_last = ti
        if i+1 == t:
            break
        print(ti, end=' ')
        x_result[i+1] = sys.f_lie(x_result[i], ui, dt, bi)

    # Plot everything as requested
    plot = 'avp'
    if plot != '':
        t = tx
        # set up our figure
        rows = len(plot) if 'b' not in plot else len(plot)+1
        fig, ax = plt.subplots(rows, 3, figsize=(8, rows*2+2))
        if rows == 1:
            ax = ax.reshape((1,3))

        i = 0
        # iterate through plotting everything
        for p in plot:
            # plot angles
            if p == "a":
                ax[i,0].set_ylabel("Angles")
                ax[i,0].set_title("Pitch")
                ax[i,0].plot(t, -np.arcsin(x[:,2,0]), label="Actual")
                ax[i,0].plot(t, -np.arcsin(x_result[:,2,0]), label="Predicted")
                ax[i,1].set_title("Roll")
                ax[i,1].plot(t, np.arctan2(x[:,2,1], x[:,2,2]), label="Actual")
                ax[i,1].plot(t, np.arctan2(x_result[:,2,1], x_result[:,2,2]), label="Predicted")
                ax[i,2].set_title("Yaw")
                ax[i,2].plot(t, np.arctan2(x[:,1,0], x[:,0,0]), label="Actual")
                ax[i,2].plot(t, np.arctan2(x_result[:,1,0], x_result[:,0,0]), label="Predicted")
                i += 1

            # plot velocity
            if p == 'v':
                ax[i,0].set_ylabel("Velocity")
                ax[i,0].set_title("X (global)")
                ax[i,1].set_title("Y (global)")
                ax[i,2].set_title("Z (global)")
                for j in range(3):
                    ax[i,j].plot(t, x[:,j,3], label="Actual")
                    ax[i,j].plot(t, x_result[:,j,3], label="Predicted")
                i += 1
                
            # plot position
            if p == 'p':
                ax[i,0].set_ylabel("Position")
                ax[i,0].set_title("X (global)")
                ax[i,0].plot(t, x[:,0,4], label="Actual")
                ax[i,0].plot(t, x_result[:,0,4], label="Predicted")
                ax[i,1].set_title("Y (global)")
                ax[i,1].plot(t, x[:,1,4], label="Actual")
                ax[i,1].plot(t, x_result[:,1,4], label="Predicted")
                ax[i,2].set_title("Z (global)")
                ax[i,2].plot(t, x[:,2,4], label="Actual")
                ax[i,2].plot(t, x_result[:,2,4], label="Predicted")
                i += 1

            # plot bias
            if p == 'b':
                for j in range(3):
                    ax[i,j].set_ylabel("Gyro Bias")
                    ax[i,j].plot(t, bias[:,0,j], label="Actual")
                    ax[i,j].plot(t, bias_result[:,0,j], label="Result")
                    ax[i+1,j].set_ylabel("Accel Bias")
                    ax[i+1,j].plot(t, bias[:,1,j], label="Actual")
                    ax[i+1,j].plot(t, bias_result[:,1,j], label="Result")
                i += 2

        ax[-1,2].legend(loc='best')
        fig.tight_layout()
        plt.show()

    