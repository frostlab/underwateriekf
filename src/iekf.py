import numpy as np
from numpy.linalg import inv
from scipy.linalg import expm
from tqdm import tqdm

class IEKF:
    def __init__(self, system, mu_0, sigma_0, left=False):
        """The newfangled Invariant Extended Kalman Filter

        Args:
            system      (class) : The system to run the iEKF on. It will pull Q, R, f, h from this.
            mu0     (nxn array) : Initial starting point of system
            sigma0  (mxm array) : Initial covariance of system"""
        if mu_0.shape == (9,):
            mu_0 = expm( system.carat(mu_0) )

        self.sys = system
        self.mus = [mu_0]
        self.sigmas = [sigma_0]
        self.biass = [np.zeros((2,3))]

        self.invR = np.zeros((3,3))
        self.invR[-1,-1] = 1 / self.sys.R[3,3]

        self.left = left
    
    # These 3 properties are helpers. Since our mus and sigmas are stored in a list
    # it can be a pain to access the last one. These provide "syntactic sugar"
    @property
    def mu(self):
        return self.mus[-1]

    @property
    def sigma(self):
        return self.sigmas[-1]

    @property
    def bias(self):
        return self.biass[-1]

    def predict(self, u, dt):
        """Runs prediction step of iEKF.

        Args:
            u       (k ndarray) : control taken at this step

        Returns:
            mu    (nxn ndarray) : Propagated state
            sigma (nxn ndarray) : Propagated covariances"""
        
        #get mubar and sigmabar
        mu_bar = self.sys.f_lie(self.mu, u, dt, self.bias)

        # make adjoint
        I = np.eye(6)
        zero = np.zeros((9,6))
        adj_X = np.block([[self.sys.adjoint(mu_bar), zero],
                          [                  zero.T,    I]])

        # make propagation matrix
        zero    = np.zeros((3,3))
        I       = np.eye(3)
        if self.left:
            w_cross = self.sys.cross( u[0] - self.bias[0] )
            a_cross = self.sys.cross( u[1] - self.bias[1] )
            self.expA = expm( np.block([[-w_cross,     zero,     zero,   -I, zero],
                                        [-a_cross, -w_cross,     zero, zero,   -I],
                                        [    zero,        I, -w_cross, zero, zero],
                                        [    zero,     zero,     zero, zero, zero],
                                        [    zero,     zero,     zero, zero, zero]])*dt ) 
            sigma_bar = self.expA @ self.sigma @ self.expA.T + self.expA@ self.sys.Q @self.expA.T * dt

        else:
            R       = mu_bar[:3,:3]
            g_cross = self.sys.cross( np.array([0, 0, -9.8]) )
            # v_cross = self.sys.cross( mu_bar[:3,3] )
            # p_cross = self.sys.cross( mu_bar[:3,4] )
            self.expA = expm( np.block([[   zero, zero, zero,         -R, zero],
                                        [g_cross, zero, zero, -adj_X[3:6,0:3],   -R],
                                        [   zero,    I, zero, -adj_X[6:9,0:3], zero],
                                        [   zero, zero, zero,       zero, zero],
                                        [   zero, zero, zero,       zero, zero]])*dt ) 
            sigma_bar = self.expA @ (self.sigma + adj_X@self.sys.Q@adj_X.T*dt) @self.expA.T

        #save for use later
        self.last_u = u
        self.mus.append( mu_bar )
        self.biass.append( self.bias )
        self.sigmas.append( sigma_bar )

        return mu_bar, sigma_bar

    def update_depth(self, z, sim_bias=True):
        """Runs correction step of iEKF.

        Args:
            z (m ndarray): measurement at this step

        Returns:
            mu    (nxn ndarray) : Corrected state
            sigma (nxn ndarray) : Corrected covariances"""
        # make H matrices
        zero    = np.zeros((3,3))
        I       = np.eye(3)    
        H = np.block([zero, zero, I, zero, zero])

        I = np.eye(6)
        zero = np.zeros((9,6))
        # convert H to Right measurement
        if not self.left:
            H = H@np.block([[self.sys.adjoint(inv(self.mu)), zero],
                            [                          zero.T,    I]])

        # put our measurements into full form
        z = np.array([self.mu[0,4], self.mu[1,4], z, 0, 1])

        # make innovation
        V = (inv(self.mu) @ z)[:3]

        R = self.mu[:3,:3]

        # make our special measurement covariance
        sig_til = inv(H@self.sigma@H.T)
        meas_cov   = sig_til - sig_til@inv(R.T@self.invR@R + sig_til)@sig_til

        K = self.sigma @ H.T @ meas_cov
        K_state = K[:9]
        K_bias = K[9:]
        if self.left:
            self.mus[-1] = self.mu @ expm( self.sys.carat(K @ V) )
        else:
            self.mus[-1] = expm( self.sys.carat(K @ V) ) @ self.mu
        if sim_bias:
            self.biass[-1] = self.bias + ( K_bias@V ).reshape((2,3))
        self.sigmas[-1] = (np.eye(15) - K @ H) @ self.sigma

        return self.mu, self.sigma

    def update_dvl(self, z, sim_bias=True):
        """Runs correction step of iEKF.

        Args:
            z (m ndarray): measurement at this step

        Returns:
            mu    (nxn ndarray) : Corrected state
            sigma (nxn ndarray) : Corrected covariances"""
        # convert dvl into correct frame
        z = self.sys.dvl_r@z + self.sys.dvl_p@(self.last_u[0] - self.bias[0])

        # make H matrices
        zero    = np.zeros((3,3))
        I       = np.eye(3)    
        H = np.block([zero, I, zero, zero, zero])

        I = np.eye(6)
        zero = np.zeros((9,6))
        # convert H1 to Left measurement
        if self.left:
            H = H@np.block([[self.sys.adjoint(self.mu), zero],
                            [                     zero.T,    I]])

        # put our measurements into full form
        z = np.array([z[0], z[1], z[2], -1, 0])

        # make innovation
        V = (self.mu @ z)[:3]

        R = self.mu[:3,:3]

        # make our special measurement covariance
        meas_cov   = inv(H@self.sigma@H.T + R@self.sys.R[:3,:3]@R.T)

        K = self.sigma @ H.T @ meas_cov
        K_state = K[:9]
        K_bias = K[9:]
        if self.left:
            self.mus[-1] = self.mu @ expm( self.sys.carat(K_state @ V) )
        else:
            self.mus[-1] = expm( self.sys.carat(K_state @ V) ) @ self.mu
        if sim_bias:
            self.biass[-1] = self.bias + ( K_bias@V ).reshape((2,3))
        self.sigmas[-1] = (np.eye(15) - K @ H) @ self.sigma

        return self.mu, self.sigma

    def iterate(self, u, tu, z1, tz1, z2, tz2, sim_bias=True, use_tqdm=True):
        """Given a sequence of observation, iterate through EKF
        
        Args:
            us (txk ndarray) : controls for each step, each of size k
            zs (txm ndarray) : measurements for each step, each of size m
            
        Returns:
            mus    (txnxn ndarray) : resulting states
            sigmas (txnxn ndarray) : resulting sigmas"""
        ui = 0
        z1i = 0
        z2i = 0
        t_range = np.linspace(0, self.sys.T/self.sys.Hz, self.sys.T)
        
        # append one at the end, so after they're done they won't keep running
        tu = np.append(tu, t_range[-1]+1)
        tz1 = np.append(tz1, t_range[-1]+1)
        tz2 = np.append(tz2, t_range[-1]+1)

        dt = 0
        for t in tqdm(t_range, disable=(not use_tqdm)):
            if tu[ui] <= t:
                if ui != 0:
                    dt = tu[ui] - tu[ui-1]
                self.predict(u[ui], dt)
                if ui < len(tu)-1:
                    ui += 1
            if tz1[z1i] <= t:
                self.update_dvl(z1[z1i], sim_bias)
                if z1i < len(tz1)-1:
                    z1i += 1
            if tz2[z2i] <= t:
                self.update_depth(z2[z2i], sim_bias)
                if z2i < len(tz2)-1:
                    z2i += 1

        return np.array(self.mus)[1:], np.array(self.biass)[1:], np.array(self.sigmas)[1:]

