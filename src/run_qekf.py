import sys, os
from system import HoveringAUV
from qekf import QEKF
import matplotlib.pyplot as plt
import numpy as np
from numpy.linalg import inv
from scipy.linalg import expm
from scipy.spatial.transform import Rotation
import argparse

def main(data, sim_bias, plot, verbose):
    np.set_printoptions(suppress=True, formatter={'float_kind':f'{{:0.{verbose}f}}'.format}) 
    # np.set_printoptions(linewidth=200)

    from src.constants import Q, R, dvl_p, dvl_r, imuHz, dvlHz, depthHz

    # DVL offsets
    dvl_p = np.array([-0.17137, 0.00922, -0.33989])
    dvl_r = Rotation.from_euler('xyz', [6, 3, 90], degrees=True).as_matrix()

    #make system
    sys = HoveringAUV(Q, R, data, dvl_p, dvl_r)
    t = sys.T

    # generate data
    (x, t), u, (bias, _), z1, z2 = sys.gen_data(t, imuHz, dvlHz, depthHz, noise=True, sim_bias=sim_bias)

    # Setup the initial robot state and covariance for the Quaternion EKF
    init_cov = np.diag([np.pi/6, np.pi/6, np.pi/6, 1, 1, 1, .1, .1, .1,
                        .005, .005, .005, .05, .05, .05])**2
    start = np.random.multivariate_normal(mean=np.zeros(9), cov=init_cov[:9,:9])
    # Setup initial guess of the robot's position
    x0 = sys.data['x'][0] @ expm( sys.carat( start ) )
    # x0 = x[0,:,:] # Overwrite the initial guess with the true robot position
    # Run the Quaternion EKF
    qekf = QEKF(sys, 
                x0, 
                init_cov)
    x_result, bias_result, sigmas = qekf.iterate(*u, *z1, *z2, sim_bias)

    # Plot everything as requested
    if plot != '':
        # set up our figure
        rows = len(plot) if 'b' not in plot else len(plot)+1
        fig, ax = plt.subplots(rows, 3, figsize=(8, rows*2+2))
        if rows == 1:
            ax = ax.reshape((1,3))

        # invert stuff if'll we'll need local frame later
        if 'p' in plot or 'v' in plot:
            x_inv = inv(x)
            x_result_inv = inv(x_result)
            x_inv[:,0:3,3:5] *= -1
            x_result_inv[:,0:3,3:5] *= -1

        i = 0
        # iterate through plotting everything
        for p in plot:
            # plot angles
            if p == "a":
                ax[i,0].set_ylabel("Angles")
                ax[i,0].set_title("Pitch")
                ax[i,0].plot(t, -np.arcsin(x[:,2,0]), label="Actual")
                ax[i,0].plot(t, -np.arcsin(x_result[:,2,0]), label="Predicted")
                ax[i,1].set_title("Roll")
                ax[i,1].plot(t, np.arctan2(x[:,2,1], x[:,2,2]), label="Actual")
                ax[i,1].plot(t, np.arctan2(x_result[:,2,1], x_result[:,2,2]), label="Predicted")
                ax[i,2].set_title("Yaw")
                ax[i,2].plot(t, np.arctan2(x[:,1,0], x[:,0,0]), label="Actual")
                ax[i,2].plot(t, np.arctan2(x_result[:,1,0], x_result[:,0,0]), label="Predicted")
                i += 1

            # plot velocity
            if p == 'v':
                ax[i,0].set_ylabel("Velocity")
                ax[i,0].set_title("X (local)")
                ax[i,1].set_title("Y (local)")
                ax[i,2].set_title("Z (local)")
                for j in range(3):
                    ax[i,j].plot(t, x_inv[:,j,3], label="Actual")
                    ax[i,j].plot(t, x_result_inv[:,j,3], label="Predicted")
                i += 1
                
            # plot position
            if p == 'p':
                ax[i,0].set_ylabel("Position")
                ax[i,0].set_title("X (local)")
                ax[i,0].plot(t, x_inv[:,0,4], label="Actual")
                ax[i,0].plot(t, x_result_inv[:,0,4], label="Predicted")
                ax[i,1].set_title("Y (local)")
                ax[i,1].plot(t, x_inv[:,1,4], label="Actual")
                ax[i,1].plot(t, x_result_inv[:,1,4], label="Predicted")
                ax[i,2].set_title("Z (global)")
                ax[i,2].plot(t, x[:,2,4], label="Actual")
                ax[i,2].plot(t, x_result[:,2,4], label="Predicted")
                i += 1

            # plot bias
            if p == 'b':
                for j in range(3):
                    ax[i,j].set_ylabel("Gyro Bias")
                    ax[i,j].plot(t, bias[:,0,j], label="Actual")
                    ax[i,j].plot(t, bias_result[:,0,j], label="Result")
                    ax[i+1,j].set_ylabel("Accel Bias")
                    ax[i+1,j].plot(t, bias[:,1,j], label="Actual")
                    ax[i+1,j].plot(t, bias_result[:,1,j], label="Result")
                i += 2

        ax[-1,2].legend(loc='best')
        fig.tight_layout()
        plt.show()

if __name__ == "__main__":
    # Parse through arguments
    parser = argparse.ArgumentParser(description="HoveringAUV IEKF")
    parser.add_argument("-d", "--data", type=str, default="data.npz", help="File to read data from. Default data.npz.")
    parser.add_argument("-b", "--sim_bias", action="store_true", help="Include if bias should be simulated and tracked.")
    parser.add_argument("-p", "--plot", type=str, default="avpb", help="Each char is a state to plot. a (angle), v (velocity), p (position), b (bias). Default avpb.")
    parser.add_argument("-v", "--verbose", type=int, default="3", help="Precision for printing. Default 3")
    args = vars(parser.parse_args())
    main(**args)