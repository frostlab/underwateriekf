from src.system import HoveringAUV
from src.iekf import IEKF
from src.qekf import QEKF

import numpy as np
from numpy.linalg import inv
from scipy.linalg import expm, block_diag
from scipy.spatial.transform import Rotation
import matplotlib

import argparse
import matplotlib.pyplot as plt
from tqdm import tqdm
import seaborn as sns
import os

def main(data, sim_bias, verbose, num):
    np.set_printoptions(suppress=True, formatter={'float_kind':f'{{:0.{verbose}f}}'.format}) 
    matplotlib.rcParams['pdf.fonttype'] = 42
    matplotlib.rcParams['ps.fonttype'] = 42

    ############################     SETUP SYSTEM    ############################
    from src.constants import Q, R, dvl_p, dvl_r, imuHz, dvlHz, depthHz

    #make system
    sys = HoveringAUV(Q, R, data, dvl_p, dvl_r)
    t = sys.Hz * 2

    # generate data
    (x, t), u, (bias, _), z1, z2 = sys.gen_data(t, imuHz, dvlHz, depthHz, noise=True, sim_bias=sim_bias)

    ############################     SETUP PLOT    ############################
    sns.set(context='paper', style='whitegrid', font_scale=0.8)

    plt.figure(1, figsize=(8, 3.5))
    hspace = [0.1, 0.1, 0.05]
    wspace = [0.054, 0.032, 0.06, 0.062, 0.04, 0.033, 0.01]
    width = (1 - sum(wspace)) / 6
    height = (1- sum(hspace) ) / 2
    ax = []
    for j in range(2):
        for i in range(6):
            ax.append( plt.axes([width*i+sum(wspace[:i+1]), height*j+sum(hspace[:j+1]), width, height]) )


    ############################     ITERATE + PLOT IEKF    ############################
    # Run the iekf
    lw = 0.5
    init_cov = np.diag([np.pi/6, np.pi/6, np.pi/6, 2, 2, 2, 1, 1, 1,
                        .005, .005, .005, .05, .05, .05])**2
    R = sys.data['x'][0][:3,:3]
    v_cross = HoveringAUV.cross( sys.data['x'][0][:3,3] )
    p_cross = HoveringAUV.cross( sys.data['x'][0][:3,4] )
    init_cov_qekf = block_diag( R.T@init_cov[:3,:3]@R,
                                init_cov[3:6,3:6] + v_cross@init_cov[:3,:3]@v_cross.T,
                                init_cov[6:9,6:9] + p_cross@init_cov[:3,:3]@p_cross.T,
                                init_cov[9:12,9:12],
                                init_cov[12:15,12:15] )

    for i in tqdm(range(num), position=1):
        start = np.random.multivariate_normal(mean=np.zeros(9), cov=init_cov[:9,:9])
        x0 = expm( sys.carat( start ) ) @ sys.data['x'][0]

        # Run IEKF
        iekf = IEKF(sys, x0, init_cov)
        x_result, bias_result, sigmas = iekf.iterate(*u, *z1, *z2, sim_bias, use_tqdm=False)
        # invert stuff if'll we'll need local frame later
        x_result_inv = inv(x_result)
        x_result_inv[:,0:3,3:5] *= -1
        # plot
        ax[0].plot(t, -np.arcsin(x_result[:,2,0])*180/np.pi, lw=lw)
        ax[1].plot(t, np.arctan2(x_result[:,2,1], x_result[:,2,2])*180/np.pi, lw=lw)
        ax[2].plot(t, x_result[:,2,4], lw=lw)
        for j in range(3):
            ax[j+3].plot(t, x_result_inv[:,j,3], lw=lw)
        x_result_iekf = x_result


        # Run QEKF
        qekf = QEKF(sys, x0, init_cov_qekf)
        x_result, bias_result, sigmas = qekf.iterate(*u, *z1, *z2, sim_bias, use_tqdm=False)
        # invert stuff if'll we'll need local frame later
        x_result_inv = inv(x_result)
        x_result_inv[:,0:3,3:5] *= -1
        # plot
        ax[6].plot(t, -np.arcsin(x_result[:,2,0])*180/np.pi, lw=lw)
        ax[7].plot(t, np.arctan2(x_result[:,2,1], x_result[:,2,2])*180/np.pi, lw=lw)
        ax[8].plot(t, x_result[:,2,4], lw=lw)
        for j in range(3):
            ax[j+9].plot(t, x_result_inv[:,j,3], lw=lw)

        results = {'x0': x0, 'iekf': x_result_iekf, 'qekf': x_result}

    ############################     PLOT GROUND TRUTH     ############################
    # invert stuff if'll we'll need local frame later
    x_inv = inv(x)
    x_inv[:,0:3,3:5] *= -1
    lw = 2
    c = 'k--'

    # Do the actual plotting
    ax[0].plot(t, -np.arcsin(x[:,2,0])*180/np.pi, c, lw=lw)
    ax[1].plot(t, np.arctan2(x[:,2,1], x[:,2,2])*180/np.pi, c, lw=lw)
    ax[2].plot(t, x[:,2,4], c, lw=lw)
    for j in range(3):
        ax[j+3].plot(t, x_inv[:,j,3], c, lw=lw)
    ax[6].plot(t, -np.arcsin(x[:,2,0])*180/np.pi, c, lw=lw)
    ax[7].plot(t, np.arctan2(x[:,2,1], x[:,2,2])*180/np.pi, c, lw=lw)
    ax[8].plot(t, x[:,2,4], c, lw=lw)
    for j in range(3):
        ax[j+9].plot(t, x_inv[:,j,3], c, lw=lw)

    # Set all titles
    ax[0].set_title("RInEKF - Pitch", weight='bold')
    ax[0].set_ylabel("degrees", weight='bold')
    ax[1].set_title("RInEKF - Roll", weight='bold')
    ax[2].set_title("RInEKF - Pos-Z", weight='bold')
    ax[2].set_ylabel("meters", weight='bold')
    ax[3].set_ylabel("m/sec", weight='bold')
    ax[3].set_title("RInEKF - Vel-X", weight='bold')
    ax[4].set_title("RInEKF - Vel-Y", weight='bold')
    ax[5].set_title("RInEKF - Vel-Z", weight='bold')

    ax[6].set_title("QEKF - Pitch", weight='bold')
    ax[6].set_ylabel("degrees", weight='bold')
    ax[7].set_title("QEKF - Roll", weight='bold')
    ax[8].set_title("QEKF - Pos-Z", weight='bold')
    ax[8].set_ylabel("meters", weight='bold')
    ax[9].set_ylabel("m/sec", weight='bold')
    ax[9].set_title("QEKF - Vel-X", weight='bold')
    ax[10].set_title("QEKF - Vel-Y", weight='bold')
    ax[11].set_title("QEKF - Vel-Z", weight='bold')

    # Move things around a bit
    for j in range(12):
        ax[j].tick_params('y', pad=-3)
        ax[j].tick_params('x', pad=-2)
    for j in range(6):
        ax[j].set_xlabel('seconds', weight='bold')
    for j in range(6,12):
        ax[j].set_xticklabels([])

    # make axis plots match
    for j in range(6):
        plt.setp(ax[j], ylim=ax[6+j].get_ylim())

    name = os.path.splitext(os.path.basename(data))[0]
    plt.savefig(f'figures/conv_{name}_n{num}.pdf', dpi=200)
    plt.show()

if __name__ == "__main__":
    # Parse through arguments
    parser = argparse.ArgumentParser(description="HoveringAUV IEKF")
    parser.add_argument("-d", "--data", type=str, default="data/data.npz", help="File to read data from. Default data.npz.")
    parser.add_argument("-b", "--sim_bias", action="store_true", help="Include if bias should be simulated and tracked.")
    parser.add_argument("-v", "--verbose", type=int, default="3", help="Precision for printing. Default 3")
    parser.add_argument("-n", "--num", type=int, default="10", help="Number of iterations to run.")
    args = vars(parser.parse_args())
    main(**args)