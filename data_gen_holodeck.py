import holodeck
import numpy as np
import argparse
from pynput import keyboard
np.set_printoptions(suppress=True,
   formatter={'float_kind':'{:0.8f}'.format}) 

def main(ticks, outfile, force, rm, not_ideal):

    ###################           SET UP HOLODECK           ###################
    if rm:
        holodeck.packagemanager.remove("Ocean")
    if "Ocean" not in holodeck.packagemanager.installed_packages():
        holodeck.packagemanager.install("Ocean", "https://robots.et.byu.edu/jenkins/job/holodeck-ocean-engine/job/develop/lastSuccessfulBuild/artifact/Ocean.zip")
    scenario = "SimpleUnderwater-AUV" if not_ideal else "SimpleUnderwater-PerfectAUV"


    ###################           SET UP CONTROLS           ###################
    def on_press(key):
        global pressed_keys
        try:
            pressed_keys.append(key.char)
            pressed_keys = list(set(pressed_keys))
        except:
            pass

    def on_release(key):
        global pressed_keys
        try:
            pressed_keys.remove(key.char)
        except:
            pass

    listener = keyboard.Listener(
        on_press=on_press,
        on_release=on_release)
    listener.start()

    def parse_keys(keys, val):
        command = np.zeros(8)
        if 'i' in keys:
            command[0:4] += val
        if 'k' in keys:
            command[0:4] -= val
        if 'j' in keys:
            command[[4,7]] += val / 4
            command[[5,6]] -= val / 4
        if 'l' in keys:
            command[[4,7]] -= val / 4
            command[[5,6]] += val / 4

        if 'w' in keys:
            command[4:8] += val
        if 's' in keys:
            command[4:8] -= val
        if 'a' in keys:
            command[[4,6]] += val
            command[[5,7]] -= val
        if 'd' in keys:
            command[[4,6]] -= val
            command[[5,7]] += val

        global record
        if 'r' in keys and record == False:
            record = True
            print("Starting Recording")

        return command


    ###################           BEGIN SIMULATION           ###################
    # set things up to save
    val = 15
    x = []
    z1 = []
    z2 = []
    u = []
    global record
    record = False
    global pressed_keys
    pressed_keys = list()

    # This is where the magic actually happens
    with holodeck.make(scenario, ticks_per_sec=ticks) as env:
        env.set_render_quality(0)
        i = 0

        while True:
            command = parse_keys(pressed_keys, force)
            if 'q' in pressed_keys:
                break
                
            #send to holodeck
            env.act("auv0", command)
            state = env.tick()

            #make state
            if record:
                temp = np.eye(5)
                temp[:4,:4] = state['PoseSensor']
                temp[:3, 4] = state['VelocitySensor']
                # flip velocity and position columns
                temp[:3, [3,4]] = temp[:3, [4,3]]

                #save stuff we'll need
                x.append(temp)
                z1.append(state['DVLSensor'])
                z2.append(state['PoseSensor'][2,3])
                u.append(state['IMUSensor'])

                i+=1 
                print(i/ticks)

    if record:
        np.savez(outfile, x=np.array(x), z1=np.array(z1), z2=np.array(z2), u=np.array(u), ticks=ticks)
        print("Data Saved.")


if __name__ == "__main__":
    # Parse through arguments
    parser = argparse.ArgumentParser(description="HoveringAUV Simulator & Data Generation")
    parser.add_argument("-o", "--outfile", type=str, default="data.npz", help="File to save data to. Defaults to data.npz.")
    parser.add_argument("-t", "--ticks", type=int, default=200, help="Number of ticks per sec to run sim at. Defaults to 100.")
    parser.add_argument("--force", type=float, default=12, help="Magnitude of thruster forces. Defaults to 12.")
    parser.add_argument("--rm", action="store_true", help="Removes and downloads environment binary. Useful for updates.")
    parser.add_argument("--not_ideal", action="store_true", help="Include to use real robot instead of ideal.")
    args = vars(parser.parse_args())
    main(**args)