from src.system import HoveringAUV
from src.iekf import IEKF
from src.qekf import QEKF

import numpy as np
from numpy.linalg import inv
from scipy.linalg import expm, block_diag
from scipy.spatial.transform import Rotation

import argparse
import matplotlib.pyplot as plt
import matplotlib
from tqdm import tqdm
import seaborn as sns
from time import time
import pandas as pd
import os

def iterate(self, u, tu, z1, tz1, z2, tz2, sim_bias=True, use_tqdm=True):
    """Given a sequence of observation, iterate through EKF
    
    Args:
        us (txk ndarray) : controls for each step, each of size k
        zs (txm ndarray) : measurements for each step, each of size m
        
    Returns:
        mus    (txnxn ndarray) : resulting states
        sigmas (txnxn ndarray) : resulting sigmas"""
    ui = 0
    z1i = 0
    z2i = 0
    t_range = np.linspace(0, self.sys.T/self.sys.Hz, self.sys.T)
    times = np.zeros((len(tu), 3))
    
    # append one at the end, so after they're done they won't keep running
    tu = np.append(tu, t_range[-1]+1)
    tz1 = np.append(tz1, t_range[-1]+1)
    tz2 = np.append(tz2, t_range[-1]+1)

    dt = 0
    for t in tqdm(t_range, disable=(not use_tqdm)):
        if tu[ui] <= t:
            if ui != 0:
                dt = tu[ui] - tu[ui-1]
            start = time()
            self.predict(u[ui], dt)
            times[ui, 0] += time() - start
            if ui < len(tu)-1:
                ui += 1
        if tz1[z1i] <= t:
            start = time()
            self.update_dvl(z1[z1i], sim_bias)
            times[z1i, 1] += time() - start
            if z1i < len(tz1)-1:
                z1i += 1
        if tz2[z2i] <= t:
            start = time()
            self.update_depth(z2[z2i], sim_bias)
            times[z2i, 2] += time() - start
            if z2i < len(tz2)-1:
                z2i += 1

    return times

def main(data, sim_bias, verbose, num):
    # np.set_printoptions(suppress=True, formatter={'float_kind':f'{{:0.{verbose}f}}'.format}) 
    matplotlib.rcParams['pdf.fonttype'] = 42
    matplotlib.rcParams['ps.fonttype'] = 42

    ############################     SETUP SYSTEM    ############################
    from src.constants import Q, R, dvl_p, dvl_r
    imuHz = 200
    dvlHz = 200
    depthHz = 200

    #make system
    sys = HoveringAUV(Q, R, data, dvl_p, dvl_r)

    ############################     SETUP PLOT    ############################
    sns.set(context='paper', style='whitegrid', font_scale=0.8)

    fig, ax = plt.subplots(1, 1, figsize=(3.5, 2))

    ############################     ITERATE + IEKF    ############################
    # Run the iekf
    init_cov = np.diag([np.pi/6, np.pi/6, np.pi/6, 2, 2, 2, 1, 1, 1,
                        .005, .005, .005, .05, .05, .05])**2
    R = sys.data['x'][0][:3,:3]
    v_cross = HoveringAUV.cross( sys.data['x'][0][:3,3] )
    p_cross = HoveringAUV.cross( sys.data['x'][0][:3,4] )
    init_cov_qekf = block_diag( R.T@init_cov[:3,:3]@R,
                                init_cov[3:6,3:6] + v_cross@init_cov[:3,:3]@v_cross.T,
                                init_cov[6:9,6:9] + p_cross@init_cov[:3,:3]@p_cross.T,
                                init_cov[9:12,9:12],
                                init_cov[12:15,12:15] )

    # generate data
    (x, t), u, (bias, _), z1, z2 = sys.gen_data(num*sys.Hz//imuHz, imuHz, dvlHz, depthHz, noise=True, sim_bias=sim_bias)

    start = np.random.multivariate_normal(mean=np.zeros(9), cov=init_cov[:9,:9])
    x0 = expm( sys.carat( start ) ) @ sys.data['x'][0]

    # run and time the IEKF
    iekf = IEKF(sys, x0, init_cov)
    times_iekf = iterate(iekf, *u, *z1, *z2, sim_bias, use_tqdm=True) * 1000

    # run and time the QEKF
    qekf = QEKF(sys, x0, init_cov_qekf)
    times_qekf = iterate(qekf, *u, *z1, *z2, sim_bias, use_tqdm=True) * 1000

    ############################     PLOT TRUTH     ############################
    temp1 = [['RInEKF']*num*3, ['Predict']*num+['Update DVL']*num+['Update Depth']*num, times_iekf.T.flatten()]
    temp2 = [['QEKF']*num*3, ['Predict']*num+['Update DVL']*num+['Update Depth']*num, times_qekf.T.flatten()]
    df = pd.DataFrame(list(zip(*temp1))+list(zip(*temp2)), columns=['Algo', '', 'Time (ms)'])

    sns.violinplot(data=df, x='', y='Time (ms)', hue='Algo', inner=None, ax=ax)
    ax.set_xticklabels(['Predict', 'Update DVL', 'Update Depth'], weight='bold')

    ax.tick_params('y', pad=-2)
    ax.tick_params('x', pad=-1)

    handles, labels = ax.get_legend_handles_labels()
    ax.legend(handles=handles[:], labels=labels[:])

    plt.ylabel('Time (ms)', weight='bold')

    plt.tight_layout()
    name = os.path.splitext(os.path.basename(data))[0]
    plt.savefig(f'figures/speed_{name}_n{num}.pdf', dpi=200)
    plt.show()

if __name__ == "__main__":
    # Parse through arguments
    parser = argparse.ArgumentParser(description="HoveringAUV IEKF")
    parser.add_argument("-d", "--data", type=str, default="data/data.npz", help="File to read data from. Default data.npz.")
    parser.add_argument("-b", "--sim_bias", action="store_true", help="Include if bias should be simulated and tracked.")
    parser.add_argument("-v", "--verbose", type=int, default="3", help="Precision for printing. Default 3")
    parser.add_argument("-n", "--num", type=int, default="1000", help="Number of iterations to run.")
    args = vars(parser.parse_args())
    main(**args)